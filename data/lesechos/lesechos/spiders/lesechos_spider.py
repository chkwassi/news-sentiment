import scrapy


class LesechosSpider(scrapy.Spider):
    name = "lesechos"
    allowed_domains = ["lesechos.fr"]
    start_urls = ["https://www.lesechos.fr/politique-societe?page=3"]

    def parse(self, response):
        urls = response.xpath(
            "//div[contains(@class, 'sc-AxjAm jdkqq3-0 jrliLE')]/article/div/a/@href"
        ).getall()
        urls = [response.urljoin(url) for url in urls]
        # urls = [url for url in urls if "/politique-societe" in url]

        for url in urls:
            yield scrapy.Request(
                url=url, callback=self.parse_url, meta={"dont_redirect": True}
            )

    def parse_url(self, response):
        yield {
            "title": response.xpath("//header//h1/text()").get(),
            "description": response.xpath("//header/p/text()").get(),
            "date": response.xpath(
                "//div[contains(@class, 'sc-AxjAm sc-1i0ieo8-1 dNkYYU')]/span/text()"
            ).get(),
            "article": "".join(
                response.xpath(
                    "//p[contains(@class, 'sc-AxirZ bUHJAM')]//text()"
                ).getall()
            ),
        }
