import scrapy


class LeparisienSpider(scrapy.Spider):
    name = "leparisien"
    allowed_domains = ["leparisien.fr"]
    start_urls = ["https://www.leparisien.fr/archives/2020/11-07-2020/"]

    def parse(self, response):
        urls = response.xpath(
            "//div[contains(@id, 'top')]/div[contains(@class, 'story-preview story-preview--oneline flex-feed-unit')]/div/a/@href"
        ).getall()
        urls = ["https:" + url for url in urls if "/politique" in url]

        for url in urls:
            yield scrapy.Request(
                url=url, callback=self.parse_url, meta={"dont_redirect": True}
            )

    def parse_url(self, response):
        yield {
            "title": response.xpath("//header//h1/text()").get(),
            "description": response.xpath("//header/h2/text()").get(),
            "article": "".join(
                response.xpath(
                    "//section[contains(@class, 'content')]/p//text()"
                ).getall()
            ),
        }
