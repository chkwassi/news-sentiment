import scrapy


class LefigaroSpider(scrapy.Spider):
    name = "lefigaro"
    allowed_domains = ["lefigaro.fr"]
    start_urls = ["https://www.lefigaro.fr/politique"]

    def parse(self, response):
        urls = response.xpath(
            "//section[contains(@class, 'e1g0wpiv5')]/div/a/@href"
        ).getall()
        urls = [response.urljoin(url) for url in urls]
        # urls = [url for url in urls if "/politique-societe" in url]

        for url in urls:
            yield scrapy.Request(
                url=url, callback=self.parse_url, meta={"dont_redirect": True}
            )

    def parse_url(self, response):
        yield {
            "title": response.xpath("//header//h1/text()").get(),
            "description": response.xpath("//header/p/text()").get(),
            "date": response.xpath(
                "//header/div[contains(@class, 'e590w5o1')]/time/@datetime"
            ).get(),
            "article": "".join(
                response.xpath("//div[contains(@class, 'ey7xkwt0')]/p//text()").getall()
            ),
        }
