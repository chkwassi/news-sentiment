import scrapy


class LemondeSpider(scrapy.Spider):
    name = "lemonde"
    allowed_domains = ["lemonde.fr"]
    start_urls = ["http://lemonde.fr/archives-du-monde/11-07-2020/"]

    def parse(self, response):
        urls = response.xpath(
            "//section[contains(@id, 'river')]/section/a/@href"
        ).getall()
        urls = [url for url in urls if "politique/article" in url]

        for url in urls:
            yield scrapy.Request(
                url=url, callback=self.parse_url, meta={"dont_redirect": True}
            )

        # all next pages
        n_pages = len(
            response.xpath('//section[@class="river__pagination"]')
            .css("a::attr(href)")
            .getall()
        )

        if n_pages > 0:
            for page in range(1, n_pages):
                next_page_url = response.urljoin(str(page + 1) + "/")
                yield scrapy.Request(next_page_url, self.parse)

    def parse_url(self, response):
        yield {
            "title": response.xpath("//header//h1/text()").get(),
            "description": response.xpath(
                "//header//p[contains(@class, 'article__desc')]/text()"
            ).get(),
            "article": "".join(
                response.xpath(
                    "//article//p[contains(@class, 'article__paragraph ')]//text()"
                ).getall()
            ),
        }

