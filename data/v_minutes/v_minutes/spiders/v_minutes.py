import scrapy


class LefigaroSpider(scrapy.Spider):
    name = "v_minutes"
    allowed_domains = ["20minutes.fr"]
    start_urls = ["https://www.20minutes.fr/archives/2020/07-11"]

    def parse(self, response):
        urls = response.xpath(
            "//div[contains(@class, 'box brick mb2')]/ul/li/a/@href"
        ).getall()
        urls = [
            url for url in urls if "/archives/2020" not in url and "politique" in url
        ]
        urls = [response.urljoin(url) for url in urls]

        for url in urls:
            yield scrapy.Request(
                url=url, callback=self.parse_url, meta={"dont_redirect": True}
            )

    def parse_url(self, response):
        yield {
            "title": response.xpath("//header//h1/text()").get(),
            "description": response.xpath("//header/span/text()").get(),
            "article": "".join(
                response.xpath(
                    "//div[contains(@class, 'lt-endor-body content')]/p//text()"
                ).getall()
            ),
        }
