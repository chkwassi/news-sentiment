import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd

df = pd.read_excel("data/news_for_app.xlsx")

### Data ###
# 1
df_to_display = df.copy()
df_to_display = df_to_display.loc[df_to_display.contribution >= 0.9]

df_to_display.drop(
    ["article", "tokens", "tokens_count", "topic", "contribution", "keywords",],
    axis=1,
    inplace=True,
)

# 2
topic_content = df.copy()
topic_content = df[["topic_name", "keywords"]].drop_duplicates().dropna()


def collect_and_join(words):
    return " ".join(list(set(" ".join(words).split(" "))))


topic_content = (
    topic_content.groupby(["topic_name"])["keywords"]
    .agg([("keywords", lambda words: collect_and_join(words))])
    .reset_index()
)

#### Figures ####

# 1
data_source_pie = pd.DataFrame(df.source.value_counts()).reset_index()
data_source_pie.columns = ["source", "total"]
data_source_pie.source = data_source_pie.replace(
    {
        "source": {
            "lefigaro": "Le Figaro",
            "lemonde": "Le Monde",
            "lesechos": "Les Echos",
            "leparisien": "Le Parisien",
        }
    }
)

source_pie_fig = go.Figure(
    data=[
        go.Pie(
            labels=data_source_pie.source.tolist(),
            values=data_source_pie.total.tolist(),
            hole=0.5,
        )
    ]
)
source_pie_fig.update_layout(title_text="Répartition par type de sources")

# 2
tokens_count_hist = px.histogram(df.tokens_count)
tokens_count_hist.update_layout(
    title_text="Répartition du nombre de termes par article", showlegend=False
)

# 3
topics_count = df.groupby(["polarity", "topic_name"]).size().reset_index()
topics_count.columns = ["polarity", "topic_name", "total"]
topics_count.polarity = topics_count.replace(
    {"polarity": {0: "Négatif", 1: "Positif",}}
)
topics_count_bar = px.bar(topics_count, y="total", x="topic_name", color="polarity")
topics_count_bar.update_layout(
    title_text="Total d'articles par topic et polarité", showlegend=True
)

colors = {"background": "#111111", "text": "#7FDBFF"}


### App ###
external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(
    children=[
        html.Div(
            className="row",
            children=[
                html.Div(className="two columns div-user-controls",),
                html.Div(
                    className="twelve columns div-for-charts bg-grey",
                    children=[
                        html.H4("News Dashboard"),
                        html.Div(
                            className="row",
                            children=[
                                html.Div(
                                    children=[
                                        html.Div(
                                            className="five columns div-for-charts bg-grey",
                                            children=[
                                                dcc.Graph(
                                                    id="source-pie-fig",
                                                    figure=source_pie_fig,
                                                )
                                            ],
                                        ),
                                    ],
                                ),
                                html.Div(
                                    html.Div(
                                        className="seven columns div-for-charts bg-grey",
                                        children=[
                                            html.Label("Polarité"),
                                            dcc.RadioItems(
                                                id="polarity_filter",
                                                options=[
                                                    {"label": "Tous", "value": 2,},
                                                    {"label": "Positif", "value": 1,},
                                                    {"label": "Négatif", "value": 0,},
                                                ],
                                                value=2,
                                                labelStyle={"display": "inline-block"},
                                            ),
                                            html.Label("Source"),
                                            dcc.Dropdown(
                                                id="source_filter",
                                                options=[
                                                    {
                                                        "label": "Le Monde",
                                                        "value": "lemonde",
                                                    },
                                                    {
                                                        "label": "Le Figaro",
                                                        "value": "lefigaro",
                                                    },
                                                    {
                                                        "label": "Le Parisien",
                                                        "value": "leparisien",
                                                    },
                                                    {
                                                        "label": "Les Echos",
                                                        "value": "lesechos",
                                                    },
                                                ],
                                                value=[
                                                    "lemonde",
                                                    "leparisien",
                                                    "lesechos",
                                                    "lefigaro",
                                                ],
                                                multi=True,
                                            ),
                                            html.Label("Sujet"),
                                            dcc.Dropdown(
                                                id="topic_filter",
                                                options=[
                                                    {
                                                        "label": "Remaniement",
                                                        "value": "remaniement",
                                                    },
                                                    {
                                                        "label": "Écologie",
                                                        "value": "ecologie",
                                                    },
                                                    {
                                                        "label": "Élections",
                                                        "value": "elections",
                                                    },
                                                    {
                                                        "label": "Autre",
                                                        "value": "autre",
                                                    },
                                                ],
                                                value=[
                                                    "remaniement",
                                                    "ecologie",
                                                    "elections",
                                                    "autre",
                                                ],
                                                multi=True,
                                            ),
                                            html.H2(" "),
                                            html.Div(id="news_table",),
                                        ],
                                    )
                                ),
                            ],
                        ),
                    ],
                ),
            ],
        ),
        html.H2(" "),
        html.H2(" "),
        html.H2(" "),
        html.Div(
            className="row",
            children=[
                html.Div(className="two columns div-user-controls",),
                html.Div(
                    className="four columns div-for-charts bg-grey",
                    children=[
                        dcc.Graph(id="tokens_count-hist", figure=tokens_count_hist),
                    ],
                ),
                html.Div(
                    className="four columns div-for-charts bg-grey",
                    children=[
                        dcc.Graph(id="topic_count_bar", figure=topics_count_bar),
                    ],
                ),
                html.Div(
                    className="three columns div-for-charts bg-grey",
                    children=[
                        dash_table.DataTable(
                            style_data={"whiteSpace": "normal", "height": "auto",},
                            data=topic_content.to_dict("records"),
                            columns=[
                                {"id": c, "name": c} for c in topic_content.columns
                            ],
                            page_size=2,
                        )
                    ],
                ),
            ],
        ),
    ],
)


### App Callback ###


@app.callback(
    Output("news_table", "children"),
    [
        Input("source_filter", "value"),
        Input("topic_filter", "value"),
        Input("polarity_filter", "value"),
    ],
)
def update_table(selected_source, selected_topic, selected_polarity):

    # filter from the source
    if type(selected_source) == list:
        dff = pd.DataFrame(columns=list(df.columns))
        for source in selected_source:
            source_data = df[df.source == source]
            dff = pd.concat([dff, source_data], axis=0, ignore_index=True)
    else:
        dff = df[df.source == selected_source]

    # filter from the topic
    if type(selected_topic) == list:
        dff_topic = pd.DataFrame(columns=list(df.columns))
        for topic in selected_topic:
            topic_data = dff[dff.topic_name == topic]
            dff_topic = pd.concat([dff_topic, topic_data], axis=0, ignore_index=True)
    else:
        dff_topic = dff[dff.topic_name == selected_topic]

    # filter from polarity
    if selected_polarity == 2:
        dff_polarity = dff_topic
    else:
        dff_polarity = dff_topic[dff_topic.polarity == selected_polarity]

    return dash_table.DataTable(
        id="filtered_table",
        style_data={"whiteSpace": "normal", "height": "auto",},
        data=dff_polarity.to_dict("records"),
        columns=[{"id": c, "name": c} for c in df_to_display.columns],
        page_size=4,
    )


if __name__ == "__main__":
    app.run_server(debug=True)
